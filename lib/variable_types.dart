import 'dart:math';

void main() {
  double piTest = pi;
  print(piTest);
}

void functionsSyntax() {
  var flybyObjects = ['Jupiter', 'Saturn','Saturn3', 'Uranus', 'Neptune'];
  flybyObjects.where((name) => name.contains('turn')).forEach(print);
}

int fibonacci(int n){
  if(n == 0 || n == 1){
    return n;
  }
  return fibonacci(n - 1) + fibonacci(n - 2);
}

void controlFlowStatements() {
   var name = 'Voyager I';
  var year = 1977;
  var antennaDiameter = 3.7;
  var flybyObjects = ['Jupiter', 'Saturn', 'Uranus', 'Neptune'];
  var image = {
    'tags': ['saturn'],
    'url': '//path/to/saturn.jpg'
  };

  if(year >= 2001){
    print('21st century');
  }else if(year >= 1901){
    print('20th century');
  }

  for(var object in flybyObjects){
    print(object);
  }

  for(int month = 1; month <= 12; month++){
    print(month);
  }

  while(year < 2016){
    year += 1;
    print(year);
  }
}

void stringsAndInterger() {
  int one = int.parse('1');
  assert(one == 1);
  print(one);

  double onePoint = double.parse('1.1');
  assert(onePoint == 1.1);
  print(onePoint);

  String oneAsString = 1.toString();
  assert(oneAsString == '1');
  print(oneAsString);

  String piAsString = 3.1244.toStringAsFixed(2);
  print('piAsString' + '  ' + piAsString);

  // string interpolation

  var varName = 'str';
  assert('Dart has $varName' == 'Dart has str');
  assert('Dart has ${varName.toUpperCase()}' == 'Dart has STR');
}

void createFinalAndCost() {
  final name = 'Bob';
  final String nickName = 'Bobby';
  // name = 'Alicae'; //error
  const bar = 100;
  const double atm = 2.5;
  print(bar * atm);

  var foo = [1, 2, 3];
  print(foo);
}

void variableTypesTest() {
  var name = 'Voyager I';
  var year = 1977;
  var antennaDiameter = 3.7;
  var flybyObjects = ['Jupiter', 'Saturn', 'Uranus', 'Neptune'];
  var image = {
    'tags': ['saturn'],
    'url': '//path/to/saturn.jpg'
  };
  print(name);
  print(flybyObjects);
  print(image);

  int lineCount = 0;
  assert(lineCount == null);
}

void printHello() {
  print('Hello, World');
}

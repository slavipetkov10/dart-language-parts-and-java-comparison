import 'package:simple_dart_class/classes_examples/interface_and_abstract_class/describable.dart';

class Description extends Describable{
  @override
  void describe() {
    print("from description");
  }
}

main(){
  Description description = new Description();
  description.describe();
  description.describeWithEmphasis();

  Describable desc = new Description();
  desc.describe();
  desc.describeWithEmphasis();
}
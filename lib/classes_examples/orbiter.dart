
import 'space_craft.dart';

class Orbiter extends Spacecraft{
  double altitude;

  Orbiter(String name, DateTime launchDate, this.altitude)
      : super(name, launchDate);

  void printAltitude(){
    print('Altitude: ' + this.altitude.toString() );
    print(' Name: ' + name );
    print('Launch date' + launchDate.toString());
  }
}

main(){
  Orbiter orbiter = new Orbiter("name", DateTime.now(), 4.4);
  orbiter.printAltitude();
}
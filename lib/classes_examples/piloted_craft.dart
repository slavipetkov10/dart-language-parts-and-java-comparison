import 'package:simple_dart_class/classes_examples/piloted.dart';
import 'package:simple_dart_class/classes_examples/space_craft.dart';

class PilotedCraft extends Spacecraft with Piloted{
  PilotedCraft(String name, DateTime launchDate)
      : super(name, launchDate);

}

main(){
  PilotedCraft pilotedCraft = new PilotedCraft("TestName", DateTime.now());
  print(pilotedCraft.astronauts);
  print(pilotedCraft.launchDate);
  print(pilotedCraft.name);

}